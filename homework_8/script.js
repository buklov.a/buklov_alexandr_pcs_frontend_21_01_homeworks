"use strict";
const fib = (() => {
    let a = 0;
    let b = 1;
    let c;
    return () => {
        c = a + b;
        a = b;
        b = c;
        console.log(a);
        return a;
    }
})()

fib();
fib();
fib();
fib();
fib();
fib();